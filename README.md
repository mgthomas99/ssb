# ssb

Command line utility for streaming data to/from a serial port.

## License

See the `LICENSE` file for license information.
