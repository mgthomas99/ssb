const del = require("del");
const gulp = require("gulp");
const typescript = require("gulp-typescript");

const ts_project = typescript.createProject("tsconfig.json");

gulp.task("build", function () {
  return gulp.src("src/**/*.ts")
      .pipe(ts_project())
      .pipe(gulp.dest("build/"));
});

gulp.task("clean", function () {
  return del([
    "build/",
    "log/"
  ]);
});

gulp.task("default", gulp.parallel("build"));
